#pragma once
#include "types.h"

#include "GameContainers\gw_array.h"

#include "Constants\Constants.h"

#include "GameEntities\Position.h"
#include "GameEntities\Agent.h"
#include "GameEntities\Attribute.h"
#include "GameEntities\Camera.h"
#include "GameEntities\Friendslist.h"
#include "GameEntities\Guild.h"
#include "GameEntities\Hero.h"
#include "GameEntities\Item.h"
#include "GameEntities\Map.h"
#include "GameEntities\NPC.h"
#include "GameEntities\Party.h"
#include "GameEntities\Pathing.h"
#include "GameEntities\Player.h"
#include "GameEntities\Quest.h"
#include "GameEntities\Skill.h"
#include "GameEntities\Title.h"
#include "GameEntities\Merchant.h"
#include "GameEntities\GameServer.h"

#include "GameThread.h"

#include "GWContext\GameContext.h"

namespace GW {

	typedef void(__fastcall* SendPacketGS_t)(void*, __uint32, void*);
	typedef void(__fastcall* PlayerMouseMove_t)(GamePos*);
	typedef void(__fastcall* ChangeTarget_t)(AgentID, __uint32);
	typedef void(__fastcall* SetOnlineStatus_t)(__uint32);
	typedef void(__fastcall* OpenDataWindow_t)(__uint32*);

	struct GWCACtx {
		struct DataCtx {

			gw_array<Agent*>*			agentarray;
			__uint32*					playerid;
			__uint32*					targetid;
			__uint32*					mouseoverid;
			Constants::InstanceType*	instancetype;
			GameContext*				context;
			__uint32*					mapid;
			Camera*						camera;
			FriendList*					friendslist;
			HWND*						windowhandle;
			Skill*						skilldata;
			GameServer*					gamesrv;
		}*gamedata;

		struct FuncCtx {
			SendPacketGS_t		sendpacket;
			PlayerMouseMove_t	move;
			ChangeTarget_t		changetarget;
			SetOnlineStatus_t	setonlinestatus;
			OpenDataWindow_t	opendatawindow;
		}*funcs;
		GameThread* gamethread;
	};

	typedef GWCACtx*(*GetGWCAContext_t)();

}


