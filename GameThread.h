#pragma once

#include <functional>

namespace GW {

	class GameThread {
	public:
		virtual void Enqueue(std::function<void()>& f)			= 0;
		virtual void EnqueuePermanent(std::function<void()>& f) = 0;
	};
}