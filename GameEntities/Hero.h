#pragma once

#include <Windows.h>

#include "..\Constants\Constants.h"
#include "..\GameContainers\gw_array.h"

namespace GW {

	struct HeroFlag {
		GW::Constants::HeroID hero;
		AgentID heroid;
		DWORD unk1;
		GW::Constants::HeroState state;
		GamePos flag;
		DWORD unk2;
		AgentID lockedtargetid;
	};

	using HeroFlagArray = gw_array<HeroFlag>;
}
