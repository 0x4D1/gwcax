#pragma once

#include <Windows.h>

namespace GW {

	// v1 hooker by 4D 1
	class Hook {
		BYTE* retour_func_;
		BYTE* source_;
		DWORD length_;
	public:
		BYTE* Detour(void* source, void* detour);
		void Retour();
	};


	class MemoryPatcher {
	public:
		MemoryPatcher(LPVOID addr, BYTE *patch, UINT size);
		~MemoryPatcher();

		bool TooglePatch(bool flag);
		bool TooglePatch() { TooglePatch(!flag); };

		bool GetPatchState() { return flag; };
	private:
		LPVOID addr;

		BYTE *patch;
		BYTE *backup;
		UINT size;

		bool flag;
	};

	// class PatternScanner
	// 32 bit pattern scanner for x86 programs.
	// Credits to Zat & Midi12 @ unknowncheats.me for the functionality of this class.
	class PatternScanner {
	public:

		// Initializer to determine scan range.
		PatternScanner(uintptr_t _start, uintptr_t _size);
		PatternScanner(char* moduleName = NULL);
		PatternScanner(HMODULE _module);

		// Actual pattern finder.
		void* FindPattern(char* pattern, char* mask = NULL, int offset = 0,bool deref = false);

	private:
		uintptr_t base_;
		uintptr_t size_;
	};

}