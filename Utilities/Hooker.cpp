#include "..\Utilities.h"

extern "C" {
#include "..\disasm\ld32.h"
}

void GW::Hook::Retour() {
	DWORD old_protection;

	VirtualProtect(source_, length_, PAGE_READWRITE, &old_protection);

	memcpy(source_, retour_func_, length_);

	VirtualProtect(source_, length_, old_protection, &old_protection);

	free(retour_func_);
}

BYTE* GW::Hook::Detour(void* _source, void* _detour) {
	DWORD old_protection;

	source_ = (BYTE*)_source;
	length_ = 0;
	BYTE* src = source_;
	DWORD current_op;

	do {
		current_op = length_disasm((void*)src);
		if (current_op != 0) {
			length_ += current_op;
			src += current_op;
		}
	} while (length_ < 5);

	retour_func_ = (BYTE*)malloc(length_ + 5);
	VirtualProtect(retour_func_, length_ + 5, PAGE_EXECUTE_READWRITE, &old_protection);

	memcpy(retour_func_, source_, length_);

	retour_func_ += length_;

	retour_func_[0] = 0xE9;
	*(DWORD*)(retour_func_ + 1) = (DWORD)((source_ + length_) - (retour_func_ + 5));

	VirtualProtect(source_, length_, PAGE_EXECUTE_READWRITE, &old_protection);

	source_[0] = 0xE9;
	*(DWORD*)(source_ + 1) = (DWORD)((BYTE*)_detour - (source_ + 5));

	if (length_ != 5)
		for (DWORD i = 5; i < length_; i++)
			source_[i] = 0x90;

	VirtualProtect(source_, length_, old_protection, &old_protection);

	retour_func_ -= length_;

	return retour_func_;
}
