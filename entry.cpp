#include <Windows.h>
#include <queue>
#include <vector>

#include "GWCA_DLL.h"
#include "Utilities.h"

GW::GWCACtx* g__context = nullptr;
void**		 g__baseptr = nullptr;

typedef void(__fastcall* GameThread_t)(void*);
class GameThread_Internal : public GW::GameThread {
public:
	virtual void Enqueue(std::function<void()>& f) override {
		calls_.push(f);
	}
	virtual void EnqueuePermanent(std::function<void()>& f) override {
		callsp_.push_back(f);
	}

	std::queue<std::function<void()>>	calls_;
	std::vector<std::function<void()>>	callsp_;
}g__gamethread;

GameThread_t gwca_GameThread_tramp;
void __fastcall gwca_GameThread(void* unk)
{
	if (!g__gamethread.calls_.empty())
	{
		auto& call = g__gamethread.calls_.back();
		call();
		g__gamethread.calls_.pop();
	}

	if (!g__gamethread.callsp_.empty())
	{
		for (int i = 0; i < g__gamethread.callsp_.size(); ++i) {
			g__gamethread.callsp_[i]();
		}
	}

	return gwca_GameThread_tramp(unk);
}

#define OFFSET(type,ptr,offset) (##type##)((char*)##ptr## + ##offset##)
#define DEREF(type,ptr,offset) ptr = (##type##*)(*(char**)##ptr## + ##offset##)

GW::GWCACtx* CreateContext()
{
	GW::GWCACtx* ctx	= new GW::GWCACtx;
	ctx->funcs			= new GW::GWCACtx::FuncCtx;
	ctx->gamedata		= new GW::GWCACtx::DataCtx;
	ctx->gamethread		= &g__gamethread;

	GW::PatternScanner scan(0x401000, 0x49A000);

	char* tmp;
	// functions
	ctx->funcs->sendpacket		= (GW::SendPacketGS_t)scan.FindPattern("\x55\x8B\xEC\x83\xEC\x2C\x53\x56\x57\x8B\xF9\x85");
	ctx->funcs->move			= (GW::PlayerMouseMove_t)scan.FindPattern("\xD9\x07\xD8\x5D\xF0\xDF\xE0\xF6\xC4\x01", NULL, -0x12);
	ctx->funcs->changetarget	= (GW::ChangeTarget_t)scan.FindPattern("\x33\xC0\x3B\xDA\x0F\x95\xC0\x33", NULL, -0x78);
	ctx->funcs->opendatawindow	= (GW::OpenDataWindow_t)scan.FindPattern("\x89\x45\xF4\x8B\x41\x0C\x8D", NULL, -0x0C);
	ctx->funcs->setonlinestatus = (GW::SetOnlineStatus_t)scan.FindPattern("\x8B\xCE\x89\x35\x00\x00\x00\x00\xE8\x00\x00\x00\x00\x8B\xC8", "xxxx????x????xx", -0x1C);

	// data
	ctx->gamedata->agentarray = (GW::gw_array<GW::Agent*>*)scan.FindPattern("\x6A\x00\x6A\x02\x56\x89", NULL, -0x04, true);
	ctx->gamedata->playerid = OFFSET(__uint32*, ctx->gamedata->agentarray, -0x54);
	ctx->gamedata->targetid = OFFSET(__uint32*, ctx->gamedata->agentarray, -0x500);
	ctx->gamedata->mouseoverid = OFFSET(__uint32*, ctx->gamedata->agentarray, -0x4F4);
	ctx->gamedata->instancetype = OFFSET(GW::Constants::InstanceType*, ctx->gamedata->agentarray, -0xF0);

	ctx->gamedata->camera = (GW::Camera*)scan.FindPattern("\x8B\x45\x0C\x50\x52\x51\x8B", NULL, +0x0B, true);
	ctx->gamedata->gamesrv = *(GW::GameServer**)scan.FindPattern("\x56\x33\xF6\x3B\xCE\x74\x0E", NULL, -0x04, true);
	ctx->gamedata->mapid = (__uint32*)scan.FindPattern("\xB0\x7F\x8D\x55", NULL, +0x46, true);
	ctx->gamedata->windowhandle = (HWND*)scan.FindPattern("\x56\x8B\xF1\x85\xC0\x89\x35", NULL, 0x07, true);
	ctx->gamedata->skilldata = (GW::Skill*)scan.FindPattern("\x8D\x04\xB6\x5E\xC1\xE0\x05\x05", NULL, 0x08, true);
	ctx->gamedata->friendslist = (GW::FriendList*)scan.FindPattern("\x8D\x56\x08\x8D\x4E\x30", NULL, 0x0D, true);


	g__baseptr = (void**)scan.FindPattern("\x85\xC0\x75\xF0\xC7", NULL, +0x06, true);
	if (g__baseptr) {
		void** gctx = g__baseptr;
		DEREF(void*, gctx, 0x18);
		ctx->gamedata->context = *(GW::GameContext**)gctx;
	}

	return ctx;
}

extern "C" __declspec(dllexport) void* GetGWCAContext() { return g__context; }


BOOL WINAPI DllMain(HMODULE hModule, DWORD dwReason, LPVOID)
{
	DisableThreadLibraryCalls(hModule);
	switch (dwReason) {
	case DLL_PROCESS_ATTACH:
		g__context = CreateContext();
		if (g__baseptr) {
			DEREF(void*, g__baseptr, 0x3C);
			DEREF(void*, g__baseptr, 0x04);

			gwca_GameThread_tramp = *(GameThread_t*)g__baseptr;
			*(GameThread_t*)g__baseptr = gwca_GameThread;
		}
		break;
	case DLL_PROCESS_DETACH:
		*(GameThread_t*)g__baseptr = gwca_GameThread_tramp;
		break;
	}
	return TRUE;
}